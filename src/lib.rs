extern crate core;

pub mod addr_space;
pub mod data_source;

#[cfg(test)]
mod tests {
    use super::addr_space::*;
    use super::data_source::*;
    use std::sync::Arc;

    #[test]
    fn test_main() {
        unimplemented!()
    }
}
