use core::fmt;
use core::mem;
use core::ops::Range;
use std::sync::Arc;

use bitflags::bitflags;
use scapegoat::SgMap;
use thiserror::Error;

use crate::data_source::DataSource;

////////////////////////////////////////////////////////////////

/// The type for representing a virtual address.
pub type VAddr = usize;

////////////////////////////////////////////////////////////////

bitflags! {
    /// Flags that modify the behavior of a [Mapping].
    #[derive(Clone, Copy, PartialEq, Eq, Hash, Default)]
    pub struct Flags: u8 {
        const R = 0b00000001; // Read
        const W = 0b00000010; // Write
        const X = 0b00000100; // Execute
        const C = 0b00001000; // CoW
        const S = 0b00010000; // Shared
    }
}

////////////////////////////////////////////////////////////////

/// The internal value type used by the mappings data structure. [Mapping] is a
/// nice KV-pair type that gets exposed to the public, this is just the value
/// type (the key is the [VAddr]). This is a (slightly nicer) alternative to
/// using an ordered set and implementing [Ord] for [Mapping].
#[derive(Clone)]
struct MappingValue {
    source: Arc<dyn DataSource>,
    range: Range<usize>,
    flags: Flags,
}

/// A mapping from a [VAddr] range to a range of equal length in a [DataSource].
pub struct Mapping<'a>(VAddr, &'a MappingValue);

impl Mapping<'_> {
    /// Returns the mapping's [DataSource].
    pub fn source(&self) -> &Arc<dyn DataSource> {
        &self.1.source
    }

    /// Returns the source range covered by the mapping.
    pub fn source_range(&self) -> Range<usize> {
        self.1.range.clone()
    }

    /// Returns the address range covered by the mapping.
    pub fn addr_range(&self) -> Range<VAddr> {
        self.0..self.0 + self.1.range.len()
    }

    /// Returns the mapping's [Flags].
    pub fn flags(&self) -> Flags {
        self.1.flags
    }
}

////////////////////////////////////////////////////////////////

/// The error type for [AddrSpace::add_mapping] and [AddrSpace::add_mapping_at].
#[derive(Error, Debug)]
pub enum AddMappingError {
    /// Returned when the mapping has zero length.
    #[error("mapping has a length of 0")]
    Empty,

    /// Returned when the mapping has an `addr`, `range.start`, or
    /// `range.end` that is not a multiple of `page_size`.
    #[error("mappings must be aligned to {0} bytes")]
    Misaligned(usize),

    /// Returned by [AddrSpace::add_mapping_at] when adding the mapping at the
    /// specified [VAddr] would cause it to overlap with another mapping.
    #[error("mappings would overlap")]
    WouldOverlap,

    /// Returned by [AddrSpace::add_mapping_at] when the mapping is not in the
    /// range `[addr_min, addr_max)`.
    #[error("mapping is outside of allowed range")]
    OutOfRange,

    /// Returned by [AddrSpace::add_mapping] when space cannot be found for the
    /// mapping.
    #[error("cannot find space for mapping")]
    CannotFindSpace,

    /// Returned when the underlying data structure used for mappings is full.
    #[error("internal mappings data structure is full")]
    TooManyMappings,
}

////////////////////////////////////////////////////////////////

/// A struct containing [AddrSpace] parameters.
#[derive(Clone, Debug)]
pub struct AddrSpaceConfig {
    /// The allowed range for [VAddr]s.
    pub addr_range: Range<VAddr>,
    /// The page size (so, the required alignment for [Mapping]s), in bytes.
    pub page_size: usize,
    /// The minimum number of free pages to leave on either side when using
    /// [AddrSpace::add_mapping]. Pages can always be mapped without gaps when
    /// using [AddrSpace::add_mapping_at].
    pub gap_pages: usize,
}

impl Default for AddrSpaceConfig {
    fn default() -> Self {
        Self {
            addr_range: 0..(1 << 38),
            page_size: 4096,
            gap_pages: 1,
        }
    }
}

////////////////////////////////////////////////////////////////

/// A struct representing a virtual address space with up to `N` [Mapping]s.
pub struct AddrSpace<const N: usize> {
    name: String,
    config: AddrSpaceConfig,
    mappings: SgMap<VAddr, Option<MappingValue>, N>,
}

impl<const N: usize> AddrSpace<N> {
    /// Creates a new [AddrSpace].
    pub fn new(name: String, config: AddrSpaceConfig) -> Self {
        assert!(
            config.addr_range.start % config.page_size == 0
                && config.addr_range.end % config.page_size == 0,
            "address range start/end must be aligned"
        );

        Self {
            name,
            config,
            mappings: Default::default(),
        }
    }

    /// Returns the name for this [AddrSpace].
    pub fn name(&self) -> &str {
        &self.name
    }

    /// Returns an iterator over all of the mappings.
    pub fn mappings(&self) -> impl Iterator<Item = Mapping> {
        self.mappings
            .iter()
            .map(|(&addr, mapping)| Mapping(addr, mapping.as_ref().unwrap()))
    }

    /// Returns an iterator over all of the mappings that overlap the specified
    /// range.
    pub fn mappings_range(&self, range: Range<VAddr>) -> impl Iterator<Item = Mapping> {
        // Get the last mapping that starts before the range, if any.
        let prev_mapping = self.mappings.range(..range.start).last();
        // Check if the end of the mapping is past the start of the range.
        let prev_mapping = prev_mapping
            .map(|(&addr, mapping)| Mapping(addr, mapping.as_ref().unwrap()))
            .filter(|mapping| mapping.addr_range().end > range.start);

        // Get all of the mappings that start in the range.
        let next_mappings = self
            .mappings
            .range(range)
            .map(|(&addr, mapping)| Mapping(addr, mapping.as_ref().unwrap()));

        prev_mapping.into_iter().chain(next_mappings)
    }

    /// Returns an iterator over all of the free [VAddr] ranges.
    pub fn free_space(&self) -> impl Iterator<Item = Range<VAddr>> + '_ {
        // Include the space before the first mapping, if any.
        let space_before = self
            .mappings()
            .next()
            .map(|mapping| self.config.addr_range.start..mapping.addr_range().start);

        // Include the space after the last mapping, if any.
        let space_after = self
            .mappings()
            .last()
            .map(|mapping| mapping.addr_range().end..self.config.addr_range.end);

        // Include the space between the mappings.
        let space_between = self
            .mappings()
            .zip(self.mappings().skip(1))
            .map(|(a, b)| a.addr_range().end..b.addr_range().start);

        space_before
            .into_iter()
            .chain(space_between)
            .chain(space_after.into_iter())
            .filter(|range| !range.is_empty())
    }

    /// Adds a new mapping at any location.
    ///
    /// This currently picks the first available space that has `gap_pages`
    /// free pages on both sides, but could be changed later.
    pub fn add_mapping(
        &mut self,
        source: Arc<dyn DataSource>,
        range: Range<usize>,
        flags: Flags,
    ) -> Result<VAddr, AddMappingError> {
        // This is guaranteed to be aligned, because any existing pages
        // start/end on page boundaries, and guaranteed to be in-range because
        // the free ranges are all in-range and filtered by length.
        let padding = self.config.gap_pages * self.config.page_size;
        let addr = self
            .free_space()
            .find(|range| range.len() >= padding * 2)
            .map(|range| range.start + padding)
            .ok_or(AddMappingError::CannotFindSpace)?;

        self.add_mapping_with(source, range, flags, addr)
    }

    /// Adds a new mapping at the specified location.
    pub fn add_mapping_at(
        &mut self,
        source: Arc<dyn DataSource>,
        range: Range<usize>,
        flags: Flags,
        addr: VAddr,
    ) -> Result<VAddr, AddMappingError> {
        // Make sure the start and end are within the allowed range.
        let end_addr = addr
            .checked_add(range.len())
            .ok_or(AddMappingError::OutOfRange)?;
        if addr < self.config.addr_range.start || self.config.addr_range.end < end_addr {
            return Err(AddMappingError::OutOfRange);
        }

        // Make sure no mappings overlap this range.
        if self.mappings_range(addr..end_addr).next().is_some() {
            return Err(AddMappingError::WouldOverlap);
        }

        self.add_mapping_with(source, range, flags, addr)
    }

    /// Adds a new mapping, using the provided function to get the address.
    ///
    /// This assumes that the provided address is valid (so, aligned, in range,
    /// and does not cause any overlap), which is why this is private.
    fn add_mapping_with(
        &mut self,
        source: Arc<dyn DataSource>,
        range: Range<usize>,
        flags: Flags,
        addr: VAddr,
        // allocator: impl Fn(usize) -> Option<VAddr>,
    ) -> Result<VAddr, AddMappingError> {
        // Make sure the length is non-zero.
        if range.is_empty() {
            return Err(AddMappingError::Empty);
        }

        // Make sure the range start and end are both aligned.
        let page_size = self.config.page_size;
        if range.start % page_size != 0 || range.end % page_size != 0 {
            return Err(AddMappingError::Misaligned(page_size));
        }

        // Make sure there is enough space in the data structure.
        if self.mappings.is_full() {
            return Err(AddMappingError::TooManyMappings);
        }

        let entry_value = MappingValue {
            source,
            range,
            flags,
        };

        assert!(self.mappings.insert(addr, Some(entry_value)).is_none());

        Ok(addr)
    }

    /// Removes the mapping starting at the specified address.
    ///
    /// This can be paired with [mappings_range](Self::mappings_range) to remove
    /// all of the mappings in a specified range.
    pub fn remove_mapping(&mut self, addr: VAddr) -> Option<Arc<dyn DataSource>> {
        self.mappings
            .remove(&addr)
            .map(|mapping| mapping.unwrap().source)
    }
}

////////////////////////////////////////////////////////////////

impl fmt::Debug for Flags {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}{}{}{}{}",
            if self.contains(Flags::R) { "R" } else { "-" },
            if self.contains(Flags::W) { "W" } else { "-" },
            if self.contains(Flags::X) { "X" } else { "-" },
            if self.contains(Flags::C) { "C" } else { "-" },
            if self.contains(Flags::S) { "S" } else { "-" },
        )
    }
}

impl fmt::Debug for Mapping<'_> {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let fmt_usize =
            |f: &mut fmt::Formatter, x: usize| write!(f, "{x:#00$x}", mem::size_of::<usize>() * 2);

        fmt_usize(f, self.addr_range().start)?;
        write!(f, "-")?;
        fmt_usize(f, self.addr_range().end - 1)?;
        write!(f, " : ")?;
        fmt_usize(f, self.source_range().start)?;
        write!(f, "-")?;
        fmt_usize(f, self.source_range().end - 1)?;
        write!(f, " {:?} {:?}", self.flags(), self.source())
    }
}

impl<const N: usize> fmt::Debug for AddrSpace<N> {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "AddrSpace<{}>({:?}) ", N, self.name)?;
        let mappings = self
            .mappings
            .iter()
            .map(|(&addr, entry)| Mapping(addr, entry.as_ref().unwrap()));
        f.debug_list().entries(mappings).finish()
    }
}
