# CS 393 VM API

I tried to have fun with this and experiment with things, and ended up rewriting
`AddressSpace` (now `AddrSpace`), so there are a bunch of changes!

- `AddrSpace` uses `scapegoat`'s `SgMap` for the underlying storage
- `Flags` now use `bitflags`
- `AddMappingError` (using `thiserror`) has been added
- In general the `AddrSpace` API has a bunch of changes
- Pretty formatting :D

I'm tired so I'm stopping here for now, but this is **not done yet**:

- There are no tests, and things have not been tested.
  - It is probably pretty close to being good!!! But I am not done yet.
  - You can look at the pretty formatting while you wait :D
- I want to document stuff better but I'm too tired right now.
- I want to work on `DataSource` too at some point!
